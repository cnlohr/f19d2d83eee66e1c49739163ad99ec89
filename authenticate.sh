$ echo -n "username:password" | base64
$ echo -e "api.vrchat.cloud\tFALSE\t/\tFALSE\t0\tapiKey\tJlE5Jldo5Jibnk5O5hTx6XVqsJu4WJ26" > cookiejar.txt
$ curl -b cookiejar.txt -c cookiejar.txt -A "WorldMon" -H "Authorization: Basic ########################" https://api.vrchat.cloud/api/1/auth/user
{"requiresTwoFactorAuth":["totp","otp"]}
$ curl -X 'POST' \
  'https://api.vrchat.cloud/api/1/auth/twofactorauth/totp/verify' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -b cookiejar.txt -c cookiejar.txt  \
  -d '{
  "code": "######"
}'